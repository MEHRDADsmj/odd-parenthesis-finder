#include <iostream>
#include <string_view>
#include <vector>
#include <string>
#include <queue>

void ProcessText(std::string_view inputText);

int main()
{
	std::string inputText;
	std::getline(std::cin, inputText);
	ProcessText(inputText);
	system("pause");
	return 0;
}

void ProcessText(std::string_view inputText)
{
	std::vector<int> openParenthesis; // Works as a stack
	std::queue<int> closeParenthesis;
	std::vector<int> oddIndices;

	//Filling up vectors

	//Initial text processing
	//Removes ")"s at the beginning and "("s at the end of the text and saves their index as an odd index
	int indexCounter = 0;
	for (int inputCharIndex = 0; inputText[inputCharIndex] == ')'; indexCounter++)
	{
		oddIndices.push_back(indexCounter);
		inputText.remove_prefix(1);
	}

	for (int inputCharIndex = inputText.length() - 1; inputText[inputCharIndex] == '('; inputCharIndex--)
	{
		oddIndices.push_back(inputCharIndex);
		inputText.remove_suffix(1);
	}

	for (int inputCharIndex = 0; inputCharIndex < inputText.length(); inputCharIndex++)
	{
		if (inputText[inputCharIndex] == '(')
		{
			openParenthesis.push_back(inputCharIndex);
		}
		else if (inputText[inputCharIndex] == ')')
		{
			closeParenthesis.push(inputCharIndex);
		}
	}

	//Coupling vectors' members
	for (; !openParenthesis.empty() && !closeParenthesis.empty();)
	{
		openParenthesis.pop_back();
		closeParenthesis.pop();
	}

	if (openParenthesis.empty() && closeParenthesis.empty())
	{
		std::cout << "There are no odd parentheses in the text" << std::endl;
		return;
	}

	for (; !openParenthesis.empty() || !closeParenthesis.empty();)
	{
		if (!openParenthesis.empty())
		{
			oddIndices.push_back(openParenthesis.back());
			openParenthesis.pop_back();
		}
		else if (!closeParenthesis.empty())
		{
			oddIndices.push_back(closeParenthesis.front());
			closeParenthesis.pop();
		}
	}

	for (auto index : oddIndices)
	{
		std::cout << "Found odd index at: " << index << std::endl;
	}
}